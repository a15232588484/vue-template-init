module.exports = {
    "plugins": {
      "cssnano": {
        preset: "advanced",
        autoprefixer: false,
        "postcss-zindex": false,
        "postcss-reduce-idents": false
      },
      "autoprefixer": {
        browsers: ['Android > 1', 'ChromeAndroid > 1', 'FirefoxAndroid > 1', 'Samsung > 1', 'and_uc > 1', 'iOS > 1']
      },
      "postcss-px-to-viewport": {
        viewportWidth: 750, // px / 7.5 = vw（750设计稿）
        viewportHeight: 1334,
        unitPrecision: 3,
        viewportUnit: 'vw',
        minPixelValue: 1,
        mediaQuery: false,
        selectorBlackList: ['.ignore', '.hairlines', /^\.weui/, /^\.vux/],  // vw 适配忽略vux
      },
      "postcss-plugin-px2rem": { // vux rem 适配
        rootValue: 37.5, // 根字体大小
        unitPrecision: 3 // 渲染单位小数个数
      }
    }
  }
  