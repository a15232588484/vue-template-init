// 预留demo
import Demo from '@/router/subRouters/demo'

const routes = [
  ...Demo,
  {
    path: '*',
    redirect: '/hello'
  }
]

export default routes
