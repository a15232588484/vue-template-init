import mutations from '@/vuex/mutations/index'
import actions from '@/vuex/actions/index'

const openBill = {
  namespaced: true,
  state: {},
  getters: {},
  mutations: mutations(),
  actions: actions()
}

export default openBill
