import * as mutationTypes from '@/vuex/mutations/types'

// Mutation
const mutations = function (state) {
  return {
    // 同步省级地址列表
    [mutationTypes.SET_PROVINCE_ADDRESS]: function (state, options) {
      state.addressProvince = options
    }
  }
}
export default mutations
