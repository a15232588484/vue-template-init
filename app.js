// 引入http模块
var http = require('http')
// 设置端口
// 创建服务
var server = http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'})
  res.end('hello nodejs')
})

server.listen(8081)
console.log('启动成功了')
